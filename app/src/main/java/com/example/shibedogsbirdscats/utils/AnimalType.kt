package com.example.shibedogsbirdscats.utils

enum class AnimalType {
    SHIBE, CAT, BIRD
}