package com.example.shibedogsbirdscats.viewmodel.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.shibedogsbirdscats.databinding.FragmentMainPageBinding
import com.example.shibedogsbirdscats.model.ShibeRepo
import com.example.shibedogsbirdscats.model.remote.ShibeService.Companion.BIRD
import com.example.shibedogsbirdscats.model.remote.ShibeService.Companion.CAT
import com.example.shibedogsbirdscats.model.remote.ShibeService.Companion.DOG
import com.example.shibedogsbirdscats.viewmodel.ShibeVMFactory
import com.example.shibedogsbirdscats.viewmodel.ShibeViewModel


class MainPageFragment : Fragment() {
    lateinit var binding: FragmentMainPageBinding
    private lateinit var vmFactory: ShibeVMFactory
    private val viewModel by viewModels<ShibeViewModel>() { vmFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vmFactory = ShibeVMFactory(ShibeRepo(requireContext()))
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainPageBinding.inflate(inflater, container, false)
        initViews()
        return binding.root
    }

    fun initViews() {
        binding.dogs.setOnClickListener {
            val action =
                MainPageFragmentDirections.actionMainPageFragmentToFragmentPicturesPage(DOG)
            findNavController().navigate(action)
        }

        binding.cats.setOnClickListener {
            val action =
                MainPageFragmentDirections.actionMainPageFragmentToFragmentPicturesPage(CAT)
            findNavController().navigate(action)
        }

        binding.birds.setOnClickListener {
            val action =
                MainPageFragmentDirections.actionMainPageFragmentToFragmentPicturesPage(BIRD)
            findNavController().navigate(action)
        }

    }
}