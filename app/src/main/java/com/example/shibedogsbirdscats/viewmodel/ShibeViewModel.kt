package com.example.shibedogsbirdscats.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibedogsbirdscats.model.ShibeRepo
import com.example.shibedogsbirdscats.model.local.Animal
import com.example.shibedogsbirdscats.model.remote.ShibeService.Companion.BIRD
import com.example.shibedogsbirdscats.model.remote.ShibeService.Companion.CAT
import com.example.shibedogsbirdscats.model.remote.ShibeService.Companion.DOG
import kotlinx.coroutines.launch

class ShibeViewModel(val repo: ShibeRepo) : ViewModel() {
    private val _state: MutableLiveData<ShibeState> = MutableLiveData(ShibeState())
    val state: LiveData<ShibeState> get() = _state

    fun updateImageType(animal: String) = when (animal) {
        DOG -> getShibes()
        CAT -> getCats()
        BIRD -> getBirds()
        else -> {}
    }

    fun getShibes() {
        viewModelScope.launch {
            _state.value = ShibeState(isLoading = true)
            val result = repo.getShibes()
            Log.d("123", "result: $result")
            _state.value = ShibeState(items = result, isLoading = false)
        }
    }

    fun getCats() {
        viewModelScope.launch {
            _state.value = ShibeState(isLoading = true)
            val result = repo.getCats()
            _state.value = ShibeState(items = result, isLoading = false)
        }
    }

    fun getBirds() {
        viewModelScope.launch {
            _state.value = ShibeState(isLoading = true)
            val result = repo.getBirds()
            _state.value = ShibeState(items = result, isLoading = false)
        }
    }

    data class ShibeState(
        val isLoading: Boolean = false,
        val items: List<Animal> = emptyList()
    )
}