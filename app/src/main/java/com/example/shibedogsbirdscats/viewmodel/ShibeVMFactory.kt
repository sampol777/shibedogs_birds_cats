package com.example.shibedogsbirdscats.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shibedogsbirdscats.model.ShibeRepo

class ShibeVMFactory(
    private val repo:ShibeRepo
):ViewModelProvider.NewInstanceFactory() {
  override fun <T : ViewModel> create (ModelClass:Class<T>): T {
      return ShibeViewModel(repo) as T
  }
}