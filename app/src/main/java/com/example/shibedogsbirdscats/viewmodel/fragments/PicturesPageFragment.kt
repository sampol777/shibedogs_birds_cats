package com.example.shibedogsbirdscats.viewmodel.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainer
import com.example.shibedogsbirdscats.model.ShibeRepo
import com.example.shibedogsbirdscats.viewmodel.ShibeVMFactory
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibedogsbirdscats.databinding.FragmentPicturesPageBinding
import com.example.shibedogsbirdscats.view.ShibeAdpater
import com.example.shibedogsbirdscats.viewmodel.ShibeViewModel

class PicturesPageFragment : Fragment() {
    private lateinit var binding: FragmentPicturesPageBinding
    private lateinit var vmFactory: ShibeVMFactory
    private val viewModel by viewModels<ShibeViewModel>() { vmFactory }
    private val args : PicturesPageFragmentArgs by navArgs()

    private val theAdapter: ShibeAdpater by lazy {
        ShibeAdpater()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPicturesPageBinding.inflate(inflater, container, false)
        vmFactory = ShibeVMFactory(ShibeRepo(requireContext()))
        initViews()
        initObservers()
        return binding.root
    }

    fun initViews() {
        with(binding.picures) {
            layoutManager = GridLayoutManager(this@PicturesPageFragment.context, 2)
            adapter = theAdapter
        }

            binding.back.setOnClickListener{
                val action = PicturesPageFragmentDirections.actionFragmentPicturesPageToMainPageFragment()
                findNavController().navigate(action)
        }
    }

    fun initObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            Log.d("123", "initObservers: $state")
            binding.foreverSpinner.isVisible = state.isLoading
            theAdapter.updateList(state.items)
        }
        // Pass arg animal received from MainPageFragment
        viewModel.updateImageType(args.animal)
    }


}