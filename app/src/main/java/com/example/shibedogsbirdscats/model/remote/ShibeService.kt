package com.example.shibedogsbirdscats.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ShibeService {

    companion object {
        private const val BASE_URL: String = "https://shibe.online"
        private const val ENDPOINTDOGS = "/api/shibes"
        private const val ENDPOINTBIRDS = "/api/birds"
        private const val ENDPOINTCATS = "/api/cats"
        const val DOG = "dog"
        const val CAT = "cat"
        const val BIRD = "bird"
       fun getInstance(): ShibeService = Retrofit.Builder()
           .baseUrl(BASE_URL)
           .addConverterFactory(GsonConverterFactory.create())
           .build()
           .create()

       }
    @GET(ENDPOINTDOGS)
    suspend fun getShibes(@Query("count") count: Int = 10) = getStuff(ENDPOINTDOGS, count)
    @GET(ENDPOINTBIRDS)
    suspend fun getBirds(@Query("count")count:Int = 10) = getStuff(ENDPOINTBIRDS,count)
    @GET(ENDPOINTCATS)
    suspend fun getCats(@Query("count")count: Int= 10) = getStuff(ENDPOINTCATS,count)

    @GET("{endpoint}")
    suspend fun getStuff(
        @Path("endpoint") path: String,
        @Query("count") count: Int = 10
    ): List<String>
}