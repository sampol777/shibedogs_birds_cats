package com.example.shibedogsbirdscats.model

import android.content.Context
import com.example.shibedogsbirdscats.model.local.Animal
import com.example.shibedogsbirdscats.model.local.AnimalDB
import com.example.shibedogsbirdscats.model.remote.ShibeService
import com.example.shibedogsbirdscats.utils.AnimalType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShibeRepo(context: Context) {

    private val shibeService = ShibeService.getInstance()
    private val shibeDao = AnimalDB.getInstance(context).animalDao()

    suspend fun getShibes(count: Int = 10) =
        getStuff(AnimalType.SHIBE) { shibeService.getShibes(count) }

    suspend fun getBirds(count: Int = 10) =
        getStuff(AnimalType.BIRD) { shibeService.getBirds(count) }

    suspend fun getCats(count: Int = 10) =
        getStuff(AnimalType.CAT) { shibeService.getCats(count) }

    suspend fun getStuff(animalType: AnimalType, _fun: suspend () -> List<String>): List<Animal> =
        withContext(Dispatchers.IO) {
            val cachedShibes = shibeDao.getAllTypedAnimals(animalType)
            return@withContext cachedShibes.ifEmpty {
                val remoteDoggos: List<String> = _fun.invoke()
                val entities: List<Animal> = remoteDoggos.map {
                    Animal(type = animalType, image = it)
                }
                shibeDao.insertAnimal(*entities.toTypedArray())
                return@ifEmpty entities
            }
        }
}