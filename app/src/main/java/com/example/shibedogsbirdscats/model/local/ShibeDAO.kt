package com.example.shibedogsbirdscats.model.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.shibedogsbirdscats.utils.AnimalType

@Dao
interface AnimalDao {
    @Query("SELECT * FROM animal")
    suspend fun getAll(): List<Animal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnimal(vararg animal: Animal)

    @Query("SELECT * FROM Animal WHERE type = :animalType")
    suspend fun getAllTypedAnimals(animalType: AnimalType):List<Animal>


}