package com.example.shibedogsbirdscats.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.shibedogsbirdscats.utils.AnimalType

@Entity
data class Animal(
    @PrimaryKey(autoGenerate = true)
    val id:Int = 0,
    val type:AnimalType,
    val image:String
)
